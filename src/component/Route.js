import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import InfoUserPage from '../page/InfoUserPage';
import ToDoPage from '../page/ToDoPage';
import AlbumPage from '../page/AlbumPage';


const MainRouting =()=>{
    return(
    <BrowserRouter>       
     <Route path="/" component={InfoUserPage} exact={true} />
        <Route path="/users" component={InfoUserPage} exact={true} />
      <Route path="/users/:user_id/todo" component={ToDoPage} />
      <Route path="/users/:user_id/album" component={AlbumPage} />
    </BrowserRouter>
    )}

    export default MainRouting
