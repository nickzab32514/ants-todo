import React  from 'react';
import { List, Avatar, Skeleton, Input,PageHeader } from 'antd';
import './TodoPage.css';

const { Search } = Input;

class InfoUserPage extends React.Component {
    state = {
        data: [],
        users: [],
        searchUsers:""
    };

    fetchUserData = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(respose => respose.json())
            .then(data => {
                this.setState({ users: data });
            })
        .catch(error=>console.log(error))
    }

    componentDidMount() {
        this.fetchUserData()
    }


     UpdateSearch=(event)=>{
        this.setState({ searchUsers: event.target.value });
    }

    render() {
        const { users } = this.state;
        // alert(this.state.searchUsers);
        return (
                <PageHeader>
                    <div class="butoom">
                        <div class="bgMenu">
                    <h1 class="whiteF">User Page</h1>
                <Search
                    placeholder="input search user"
                    onSearch={value => this.setState({...this.state,searchUsers:value})}
                    onChange={this.UpdateSearch}
                    style={{ width: 200 }}
                />
                 </div> 
                 </div>
                <div class="todoListStyless">
                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={
                        this.state.searchUsers?
                       users.filter(info=>info.name.match(this.state.searchUsers))
                    :
                    users
                    }
                    renderItem={item => (
                        <List.Item
                            actions={[
                                <a href={'/users/' + item.id + '/todo'}>toDo</a>
                              , <a href={'/users/' + item.id + '/album'}>Album</a>]}  

                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={<div class="row">
                                        <div>Email:{item.email}</div>
                                        <div>Phone{item.phone}</div>
                                    </div>}

                                />
                            </Skeleton>

                        </List.Item>
                    )}
                />
                </div>
                </PageHeader>
        );
    }
}


// const InfoUserPage = (props) => {
//     console.log(props)
//     return (
//         <div>
//             <Search
//                 placeholder="input search text"
//                 onSearch={value => console.log(value)}
//                 style={{ width: 200 }}
//             />
//             <h1>InfoUserPage:{props.match.params.name}</h1>
//         </div>
//     )
// }
export default InfoUserPage;
