import React, { useState, useEffect } from 'react';
import { Descriptions, List, Typography, Button, Select } from 'antd';

const { Option } = Select;

const AlbumPage = (props) => {

    const [user, setUser] = useState([]);
    const [toDoList, setToDoList] = useState([]);

    const fetchAlbumrData = () => {
        const userID = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/users/' + userID)
            .then(respose => respose.json())
            .then(data => {
                setUser(data);
            })
    }

    const fetchAlbumData = () => {
        const userID = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/albums?' + userID)
            .then(respose => respose.json())
            .then(data => {
                setToDoList(data);
            })
    }

    useEffect(() => {
        fetchAlbumrData();
        fetchAlbumData();
    }, []);
    // console.log(user)


    return (
        < div >


            <Descriptions title={"Album:Infomation Of " + user.name}>
                <Descriptions.Item label="UserName">{user.name}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Address">{user.email}</Descriptions.Item>
            </Descriptions>
            <List
                header={<div class="row">
                    <th>To DoList</th>
                </div>}
                bordered
                dataSource={
                    toDoList
                }
                renderItem={(item, index) => (
                    <List.Item>
                        <div class="col-5">
                        <a href='https://jsonplaceholder.typicode.com/photos?albumId'>{item.title}</a>
                        </div>
                    </List.Item>
                )}
            />
        </div >
    )
}
export default AlbumPage;
