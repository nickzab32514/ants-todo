import React from 'react';
import './App.css';
import InfoUserPage from './page/InfoUserPage';
import { BrowserRouter, Route } from 'react-router-dom';
import toDoPage from './page/toDoPage';

class App extends React.Component {

  render() {
    return (
      <BrowserRouter>       
      <Route path="/users" component={InfoUserPage} exact={true} />
      <Route path="/users/:userid/todo" component={toDoPage} />
    </BrowserRouter>
    );
  }
}

export default App;
